package justenjoy.drivercontroller.democontroller.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;


@RestController
public class ProyectoController {
    @Autowired
    JdbcTemplate template;

    @PostMapping("/proyecto/create")
    public ResponseEntity registrarProyecto
            (@RequestParam String nom,String correo, String pasw,String telf) throws Exception {
        Connection conn = template.getDataSource().getConnection();
        String sql = "INSERT INTO USUARIO(NOMBRE,CORREO,PASSWORD,TELEFONO) VALUES (?,?,?,?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, nom);
        pst.setString(2, correo);
        pst.setString(3, pasw);
        pst.setString(4, telf);
        pst.executeUpdate();
        return new ResponseEntity("Correcto", HttpStatus.OK);

 }
}