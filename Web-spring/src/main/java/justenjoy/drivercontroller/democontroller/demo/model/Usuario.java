package justenjoy.drivercontroller.democontroller.demo.model;

public abstract class Usuario {
    String nombre;
    String correo;
    String password;
    String telefono;

    public String getCorreo() {
        return correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
