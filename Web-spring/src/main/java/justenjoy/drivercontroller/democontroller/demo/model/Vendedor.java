package justenjoy.drivercontroller.democontroller.demo.model;

import javax.validation.constraints.Null;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class Vendedor extends Usuario{
    String especializacion;
    String direccion_de_tienda;
    String nombre_de_marca;
    String tienda_virtual;
    String recomendaciones;
    ArrayList<Pedido> pedidos_hechos;

    public Vendedor(String nombre,String correo, String password, String telefono,String especializacion, String direccion_de_tienda,String nombre_de_marca, String tienda_virtual, String recomendaciones) {
        this.nombre=nombre;
        this.correo = correo;
        this.password = password;
        this.telefono = telefono;
        this.especializacion = especializacion;
        this.direccion_de_tienda = direccion_de_tienda;
        this.nombre_de_marca=nombre_de_marca;
        this.tienda_virtual = tienda_virtual;
        this.recomendaciones = recomendaciones;
        this.pedidos_hechos = null;
    }

    public void addVendedor(Connection connection) throws ClassNotFoundException {
        try {
            String sql = "INSERT INTO VENDEDOR VALUES (?,?,?,?,?,?,?,?,?)";
            PreparedStatement pst = connection.prepareStatement(sql);
            pst.setString(1,nombre);
            pst.setString(2,correo);
            pst.setString(3,password);
            pst.setString(4,telefono);
            pst.setString(5,especializacion);
            pst.setString(6,direccion_de_tienda);
            pst.setString(7,nombre_de_marca);
            pst.setString(8,tienda_virtual);
            pst.setString(9,recomendaciones);
            pst.executeUpdate();
            pst.close();

        } catch(SQLException e ){
            System.out.println("Error SQL....");
            e.printStackTrace();
        }

    }

    public String getEspecializacion() {
        return especializacion;
    }

    public void setEspecializacion(String especializacion) {
        this.especializacion = especializacion;
    }

    public String getDireccion_de_tienda() {
        return direccion_de_tienda;
    }

    public void setDireccion_de_tienda(String direccion_de_tienda) {
        this.direccion_de_tienda = direccion_de_tienda;
    }

    public String getNombre_de_marca() {
        return nombre_de_marca;
    }

    public void setNombre_de_marca(String nombre_de_marca) {
        this.nombre_de_marca = nombre_de_marca;
    }

    public String getTienda_virtual() {
        return tienda_virtual;
    }

    public void setTienda_virtual(String tienda_virtual) {
        this.tienda_virtual = tienda_virtual;
    }

    public String getRecomendaciones() {
        return recomendaciones;
    }

    public void setRecomendaciones(String recomendaciones) {
        this.recomendaciones = recomendaciones;
    }

    public ArrayList<Pedido> getPedidos_hechos() {
        return pedidos_hechos;
    }

    public void setPedidos_hechos(ArrayList<Pedido> pedidos_hechos) {
        this.pedidos_hechos = pedidos_hechos;
    }
}
