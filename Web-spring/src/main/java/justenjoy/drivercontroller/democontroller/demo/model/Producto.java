package justenjoy.drivercontroller.democontroller.demo.model;

public class Producto {

    String producto_ID;
    String seccion;
    float precio;
    int cantidad;
    String oferta_vigente;
    String evento_vigente;

    public Producto(String producto_ID, String seccion, float precio, int cantidad, String oferta_vigente, String evento_vigente) {
        this.producto_ID = producto_ID;
        this.seccion = seccion;
        this.precio = precio;
        this.cantidad = cantidad;
        this.oferta_vigente = oferta_vigente;
        this.evento_vigente = evento_vigente;
    }

    public String getSeccion() {
        return seccion;
    }

    public String getProducto_ID() {
        return producto_ID;
    }

    public void setProducto_ID(String producto_ID) {
        this.producto_ID = producto_ID;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getOferta_vigente() {
        return oferta_vigente;
    }

    public void setOferta_vigente(String oferta_vigente) {
        this.oferta_vigente = oferta_vigente;
    }

    public String getEvento_vigente() {
        return evento_vigente;
    }

    public void setEvento_vigente(String evento_vigente) {
        this.evento_vigente = evento_vigente;
    }
}
