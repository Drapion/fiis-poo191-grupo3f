package justenjoy.drivercontroller.democontroller.demo.model;

public class Evento {
    String codigo_evento;
    String fecha_inicio_evento;
    String fecha_fin_evento;

    public Evento(String codigo_evento, String fecha_inicio_evento, String fecha_fin_evento) {
        this.codigo_evento = codigo_evento;
        this.fecha_inicio_evento = fecha_inicio_evento;
        this.fecha_fin_evento = fecha_fin_evento;
    }

    public String getCodigo_evento() {
        return codigo_evento;
    }

    public String getFecha_inicio_evento() {
        return fecha_inicio_evento;
    }

    public String getFecha_fin_evento() {
        return fecha_fin_evento;
    }
}
