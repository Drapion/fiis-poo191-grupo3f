package justenjoy.drivercontroller.democontroller.demo.model;
import java.util.ArrayList;

public class Bolsa_de_compras {

    ArrayList<Producto> lista_productos;
    float costo_total;
    int bolsa_ID;

    public Bolsa_de_compras(ArrayList<Producto> lista_productos, float costo_total, int bolsa_ID) {
        this.lista_productos = lista_productos;
        this.costo_total = costo_total;
        this.bolsa_ID = bolsa_ID;
    }

    public ArrayList<Producto> getLista_productos() {
        return lista_productos;
    }

    public void setLista_productos(ArrayList<Producto> lista_productos) {
        this.lista_productos = lista_productos;
    }

    public float getCosto_total() {
        return costo_total;
    }

    public int getBolsa_ID() {
        return bolsa_ID;
    }
}
