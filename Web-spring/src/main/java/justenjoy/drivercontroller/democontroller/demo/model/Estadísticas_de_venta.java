package justenjoy.drivercontroller.democontroller.demo.model;

public class Estadísticas_de_venta {
    float ganancias;
    String cantidad_en_stock;
    String fecha_con_mas_ventas;

    public Estadísticas_de_venta(float ganancias, String cantidad_en_stock, String fecha_con_mas_ventas) {
        this.ganancias = ganancias;
        this.cantidad_en_stock = cantidad_en_stock;
        this.fecha_con_mas_ventas = fecha_con_mas_ventas;
    }

    public float getGanancias() {
        return ganancias;
    }

    public String getCantidad_en_stock() {
        return cantidad_en_stock;
    }

    public String getFecha_con_mas_ventas() {
        return fecha_con_mas_ventas;
    }
}
