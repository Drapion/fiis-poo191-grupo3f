package justenjoy.drivercontroller.democontroller.demo.model;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public Connection connection;
    /**
     * Establecemos la conexión con la base de datos <b>rapturepark</b>.
     */
    public void connectDatabase() {
        try {
            // Registramos el driver de PostgresSQL
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }
            Connection connection = null;
            // Database connect
            // Conectamos con la base de datos
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/rapturepark");

            boolean valid = connection.isValid(50000);
            System.out.println(valid ? "TEST OK" : "TEST FAIL");
            this.connection=connection;
        } catch (SQLException sqle) {
            System.out.println("Error: " + sqle);
        }
    }

    /**
     * Método para establecer la conexión a la base de datos mediante el paso de parámetros.
     *
     * @param host <code>String</code> host name or ip. Nombre del host o ip.
     * @param port <code>String</code> listening database port. Puerto en el que escucha la base de datos.
     * @param database <code>String</code> database name for the connection. Nombre de la base de datos para la conexión.
     * @param user <code>String</code> user name. Nombre de usuario.
     * @param password  <code>String</code> user password. Password del usuario.
     */
    public void connectDatabase(String host, String port, String database,
                                String user, String password) {
        String url = "";
        try {
            // We register the PostgreSQL driver
            // Registramos el driver de PostgresSQL
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }
            Connection connection = null;
            url = "jdbc:postgresql://" + host + ":" + port + "/" + database;
            // Database connect
            // Conectamos con la base de datos
            connection = DriverManager.getConnection(
                    url,
                    user, password);
            boolean valid = connection.isValid(50000);
            System.out.println(valid ? "TEST OK" : "TEST FAIL");
        } catch (SQLException sqle) {
            System.out.println("Error al conectar con la base de datos de PostgreSQL (" + url + "): " + sqle);
        }
    }

    /**
     * Testing Java PostgreSQL connection with host and port
     * Probando la conexión en Java a PostgreSQL especificando el host y el puerto.
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Main javaPostgreSQLBasic = new Main();
        javaPostgreSQLBasic.connectDatabase();
        Connection conexion = javaPostgreSQLBasic.connection;
        Vendedor vendedor1 = new Vendedor("Joseph Riquelme Monge" ,"wonderwall@hotmail.com","algebra","992980470","Ropa Verano","Av. Los Heroes" ,"Look Hot","https://classroom.google.com/w/MzE4NTI5NzQzNDBa/t/all","5 estrellas");

        vendedor1.addVendedor(conexion);

        conexion.close();




    }
}
