package justenjoy.drivercontroller.democontroller.demo.model;

public class Oferta {
    String codigo_oferta;
    String inicio_oferta;
    String fin_oferta;
    String producto_ID;

    public Oferta(String codigo_oferta, String inicio_oferta, String fin_oferta, String producto_ID) {
        this.codigo_oferta = codigo_oferta;
        this.inicio_oferta = inicio_oferta;
        this.fin_oferta = fin_oferta;
        this.producto_ID = producto_ID;
    }

    public String getFin_oferta() {
        return fin_oferta;
    }

    public void setFin_oferta(String fin_oferta) {
        this.fin_oferta = fin_oferta;
    }

    public String getCodigo_oferta() {
        return codigo_oferta;
    }

    public String getInicio_oferta() {
        return inicio_oferta;
    }

    public String getProducto_ID() {
        return producto_ID;
    }
}
