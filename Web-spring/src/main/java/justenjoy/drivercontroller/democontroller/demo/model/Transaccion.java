package justenjoy.drivercontroller.democontroller.demo.model;

public class Transaccion {
    String transaccion_ID;
    String fecha_transaccion;
    String lugar_de_transaccion;
    String hora_transaccion;
    String estado_de_transaccion;

    public Transaccion(String transaccion_ID, String fecha_transaccion, String lugar_de_transaccion, String hora_transaccion, String estado_de_transaccion) {
        this.transaccion_ID = transaccion_ID;
        this.fecha_transaccion = fecha_transaccion;
        this.lugar_de_transaccion = lugar_de_transaccion;
        this.hora_transaccion = hora_transaccion;
        this.estado_de_transaccion = estado_de_transaccion;
    }

    public String getTransaccion_ID() {
        return transaccion_ID;
    }

    public String getFecha_transaccion() {
        return fecha_transaccion;
    }

    public String getLugar_de_transaccion() {
        return lugar_de_transaccion;
    }

    public String getHora_transaccion() {
        return hora_transaccion;
    }

    public String getEstado_de_transaccion() {
        return estado_de_transaccion;
    }
}
