package justenjoy.drivercontroller.democontroller.demo.model;

public class Comprador extends Usuario {
    int edad;
    String color_y_preferencias;
    String talla;
    String metodo_de_pago;
    String direccion;

    public Comprador(String nombre,String correo, String password, String telefono, int edad, String color_y_preferencias, String talla, String metodo_de_pago, String direccion){

        this.nombre=nombre;
        this.correo = correo;
        this.password = password;
        this.telefono = telefono;
        this.edad = edad;
        this.color_y_preferencias = color_y_preferencias;
        this.talla = talla;
        this.metodo_de_pago = metodo_de_pago;
        this.direccion = direccion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getColor_y_preferencias() {
        return color_y_preferencias;
    }

    public void setColor_y_preferencias(String color_y_preferencias) {
        this.color_y_preferencias = color_y_preferencias;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getMetodo_de_pago() {
        return metodo_de_pago;
    }

    public void setMetodo_de_pago(String metodo_de_pago) {
        this.metodo_de_pago = metodo_de_pago;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
