package justenjoy.drivercontroller.democontroller.demo.model;

public class Pedido {
    int pedido_id;
    int cantidad_del_pedido;
    int prioridad_del_pedido;
    String fecha_del_pedido;

    public Pedido(int pedido_id, int cantidad_del_pedido, int prioridad_del_pedido, String fecha_del_pedido) {
        this.pedido_id = pedido_id;
        this.cantidad_del_pedido = cantidad_del_pedido;
        this.prioridad_del_pedido = prioridad_del_pedido;
        this.fecha_del_pedido = fecha_del_pedido;
    }

    public int getPedido_id() {
        return pedido_id;
    }

    public String getFecha_del_pedido() {
        return fecha_del_pedido;
    }

    public int getCantidad_del_pedido() {
        return cantidad_del_pedido;
    }

    public void setCantidad_del_pedido(int cantidad_del_pedido) {
        this.cantidad_del_pedido = cantidad_del_pedido;
    }

    public int getPrioridad_del_pedido() {
        return prioridad_del_pedido;
    }

    public void setPrioridad_del_pedido(int prioridad_del_pedido) {
        this.prioridad_del_pedido = prioridad_del_pedido;
    }
}
